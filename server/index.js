const fs = require('fs')
const papa = require('papaparse');
const getFun = require('./ipl');

const matchesPerYear = getFun.matchesPerYear;
const matchesTeamWonPerYear = getFun.matchesTeamWonPerYear;
const extraRunsPerYear = getFun.extraRunsPerYear;
const topEconomicalBowlers = getFun.topEconomicalBowlers;
const wonTossAndMatch = getFun.wonTossAndMatch;
const playerOfTheMatch = getFun.playerOfTheMatch;
const batsmanStrikeRate = getFun.batsmanStrikeRate;
const dismissedPlayer = getFun.dismissedPlayer;
const bestSuperOverEconomy = getFun.bestSuperOverEconomy;

const matchFile = fs.readFileSync('data/matches.csv', 'utf8');
const deliveryFile = fs.readFileSync('data/deliveries.csv', 'utf8');

let matches = papa.parse(matchFile, {
    header: true,
    dynamicTyping: true
}).data;

let deliveries = papa.parse(deliveryFile, {
    header: true,
    dynamicTyping: true
}).data;

const resultMatchesPerYear = matchesPerYear(matches);
const resultMatchesTeamWon = matchesTeamWonPerYear(matches);
const extraRunsPerTeamIn2016 = extraRunsPerYear(2016, matches, deliveries);
const topEconomicalBowlersIn2015 = topEconomicalBowlers(2015, matches, deliveries);
const teamWonTossAndMatches = wonTossAndMatch(matches);
const playerOfTheSeason = playerOfTheMatch(matches);
const dismissingPlayer = dismissedPlayer(deliveries);
const bestSuperOver = bestSuperOverEconomy(deliveries);
const strikeRate = batsmanStrikeRate(matches, deliveries);

fs.writeFile('public/output/matchesPerYear.json', JSON.stringify(resultMatchesPerYear), (error) => {
    if (error) {
        return(console.log(error));
    }
})

fs.writeFile('public/output/matchesTeamWonPerYear.json', JSON.stringify(resultMatchesTeamWon), (error) => {
    if (error) {
        return(console.log(error));
    }
})

fs.writeFile('public/output/extraRunsPerTeamIn2016.json', JSON.stringify(extraRunsPerTeamIn2016), (error) => {
    if (error) {
        return(console.log(error));
    }
})

fs.writeFile('public/output/topEconomicalBowlersIn2015.json', JSON.stringify(topEconomicalBowlersIn2015), (error) => {
    if (error) {
        return(console.log(error));
    }
})

fs.writeFile('public/output/teamWonTossAndMatch.json', JSON.stringify(teamWonTossAndMatches), (error) => {
    if (error) {
        return(console.log(error));
    }
})

fs.writeFile('public/output/playerOfTheMatchForSeason.json', JSON.stringify(playerOfTheSeason), (error) => {
    if (error) {
        return(console.log(error));
    }
})

fs.writeFile('public/output/batsmanStrikeRate.json', JSON.stringify(strikeRate), (error) => {
    if (error) {
        return(console.log(error));
    }
})

fs.writeFile('public/output/topDismissedPlayer.json', JSON.stringify(dismissingPlayer), (error) => {
    if (error) {
        return(console.log(error));
    }
})

fs.writeFile('public/output/bestSuperOverEconomy.json', JSON.stringify(bestSuperOver), (error) => {
    if (error) {
        return(console.log(error));
    }
})