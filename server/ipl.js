function matchesPerYear(matchData) {
    let yearlyMatches = {};

    matchData.forEach((item) => {
        let year = item.season;
        if (yearlyMatches[year] === undefined) {
            yearlyMatches[year] = 1;
        } else {
            yearlyMatches[year] += 1;   
        }
    })

    return(yearlyMatches);
}

function matchesTeamWonPerYear(matchData) {
    let teamWon = {};

    matchData.forEach((el) => {
        let year = el.season;
        let winnerTeam = el.winner;

        if (teamWon[year] === undefined) {
            teamWon[year] = {[winnerTeam]:1};
        } else if (teamWon[year][winnerTeam] === undefined) {
            teamWon[year][winnerTeam] = 1;
        } else {
            teamWon[year][winnerTeam] += 1;
        }
    });

    return(teamWon);
}

function extraRunsPerYear(year, matchData, deliveryData) {
    let extraRun = {};

    let matchYearId = matchData.filter((item) => item.season === year)
    .map((el) => el.id)

    deliveryData.forEach((el) => {
        if (matchYearId.includes(el.match_id)) {
            let team = el.bowling_team;
            let extra = el.extra_runs;

            if (extraRun[team] === undefined) {
                extraRun[team] = extra;
            } else {
                extraRun[team] += extra;
            }
        }
    })

    return(extraRun);
}

function topEconomicalBowlers(year, matchData, deliveryData) {
    let bowlers = {};
    let topBowlers = [];

    let matchYearId = matchData.filter((item) => item.season === year)
    .map((el) => el.id)

    let matchYearlyPlayed = deliveryData.filter((el) => matchYearId.includes(el.match_id));

    matchYearlyPlayed.forEach((item) => {
        let bowlerName = item.bowler;
        let run = item.total_runs;

        if (bowlers[bowlerName] === undefined) {
            if (item.wide_runs || item.noball_runs) {
                bowlers[bowlerName] = [0, run];
            } else {
                bowlers[bowlerName] = [1, run];
            }
        } else {
            if (item.wide_runs || item.noball_runs) {
                bowlers[bowlerName][1] += run; 
            } else {
                bowlers[bowlerName][0] += 1;
                bowlers[bowlerName][1] += run; 
            }
        }
    })

    for (let name in bowlers) {
        let over = Math.floor(bowlers[name][0]/6);
        let remaniningBall = bowlers[name][0]%6;
        let economyRate = over.toString() + "." + remaniningBall.toString();
        bowlers[name] = parseFloat((bowlers[name][1]/parseFloat(economyRate)).toFixed(2));
    }

    topBowlers = Object.entries(bowlers).sort((a, b)=> a[1] - b[1]).slice(0, 10);
    return(topBowlers);
}

function wonTossAndMatch(matchData) {
    let teamWon = {};

    matchData.forEach((el) => {
        let tossWin = el.toss_winner;
        let matchWin = el.winner;

        if (tossWin === matchWin) {
            if (teamWon[tossWin] === undefined) {
                teamWon[tossWin] = 1;
            } else {
                teamWon[tossWin] += 1;
            }
        }
    });

    return(teamWon);
}

function playerOfTheMatch(matchData) {
    let players = {}
    let topPlayers = {}
    let topSeasonPlayers = {}

    matchData.forEach((el) => {
        let year = el.season;
        let playerName = el.player_of_match;

        if (players[year] === undefined) {
            players[year] = {};
        }

        if (players[year][playerName] === undefined) {
            players[year][playerName] = 1;
        } else {
            players[year][playerName] += 1;
        }
    })

    for (let item in players) {
        topPlayers[item] = Object.entries(players[item])
        .sort((a, b) => b[1] - a[1]);
    }

    for (let sortItem in topPlayers) {
        topSeasonPlayers[sortItem] = [];
        for (let sortIndex = 0; sortIndex < (topPlayers[sortItem].length)-1; sortIndex++) {
            if (topPlayers[sortItem][sortIndex][1] !== topPlayers[sortItem][sortIndex + 1][1]) {
                topSeasonPlayers[sortItem].push(topPlayers[sortItem][sortIndex]);
                break;
            } else {
                topSeasonPlayers[sortItem].push(topPlayers[sortItem][sortIndex]);
            }
        }
    }

    return(topSeasonPlayers);
}

function batsmanStrikeRate(matchData, deliveryData) {
    let seasonAndId = {};
    let strikeRate = {};
    
    for (let index = 0; index < matchData.length; index++) {
        if (seasonAndId[matchData[index].season] === undefined) {
            seasonAndId[matchData[index].season] = [matchData[index].id];
        } else {
            seasonAndId[matchData[index].season].push(matchData[index].id);
        }
    }

    deliveryData.forEach((data) => {
        let id = data.match_id;
        for (let season in seasonAndId) {
            if(seasonAndId[season].includes(id)) {
                let batsman = data.batsman;
                let run = data.batsman_runs;
                if (strikeRate[season] === undefined) {
                    strikeRate[season] = {};
                    strikeRate[season][batsman] = [1, run];
                } else if (strikeRate[season][batsman] === undefined) {
                    strikeRate[season][batsman] = [1, run];
                } else {
                    strikeRate[season][batsman][0] += 1;
                    strikeRate[season][batsman][1] += run;
                }
            }
        }
    });

    for (let year in strikeRate) {
        for (let player in strikeRate[year]) {
            strikeRate[year][player] = ((strikeRate[year][player][1]/strikeRate[year][player][0]) * 100).toFixed(2);
        }
    }

    return(strikeRate);
}

function dismissedPlayer(deliveryData) {
    let  playerDismiss = {}
    let topPlayerDismiss;
    
    deliveryData.forEach((index) => {
        if(index.player_dismissed !== null) {
            let playerName = index.player_dismissed;
            if (playerDismiss[playerName] === undefined) {
                playerDismiss[playerName] = 1;
            } else {
                playerDismiss[playerName] += 1;
            }
        }
    });
    topPlayerDismiss = Object.entries(playerDismiss).sort((a, b) => b[1] - a[1]).splice(0, 1);
    
    return(topPlayerDismiss);
}

function bestSuperOverEconomy(deliveryData) {
    let bestEconomy = {}
    let bestBowler;

    deliveryData.forEach((data) => {
        if (data.is_super_over) {
            if (bestEconomy[data.bowler] === undefined) {
                if (data.wide_runs || data.noball_runs) {
                    bestEconomy[data.bowler] = [0, data.total_runs];
                } else {
                    bestEconomy[data.bowler] = [1, data.total_runs];
                }
            } else {
                if (data.wide_runs || data.noball_runs) {
                    bestEconomy[data.bowler] = [0, data.total_runs];
                } else {
                    bestEconomy[data.bowler][0] += 1;
                    bestEconomy[data.bowler][1] += data.total_runs;
                }
            }
        }
    });

    for (let name in bestEconomy) {
        let over = Math.floor(bestEconomy[name][0]/6);
        let remaniningBall = bestEconomy[name][0]%6;
        let economyRate = over.toString() + "." + remaniningBall.toString();
        bestEconomy[name] = bestEconomy[name][1]/parseFloat(economyRate);
    }
    bestBowler = Object.entries(bestEconomy).sort((a, b)=> a[1] - b[1]).slice(0, 1);
    return(bestBowler);
}

module.exports = {
    matchesPerYear,
    matchesTeamWonPerYear,
    extraRunsPerYear,
    topEconomicalBowlers,
    wonTossAndMatch,
    playerOfTheMatch,
    batsmanStrikeRate,
    dismissedPlayer,
    bestSuperOverEconomy
}